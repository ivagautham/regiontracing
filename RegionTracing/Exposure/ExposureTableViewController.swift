//
//  ExposureTableViewController.swift
//  RegionTracing
//
//  Created by Gautham Velappan on 5/19/20.
//  Copyright © 2020 Gautham Velappan. All rights reserved.
//

import UIKit

class ExposureTableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if section == 1 {
            return LocationManager.shared.isLocationEnabled ? String("You will be notified if you are exposed to COVID-19") : String("You will not be notified if you are exposed to COVID-19")
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(44.0)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        
        if indexPath.section == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "StatusTableViewCell", for: indexPath)
            
            cell.textLabel?.text = "Location Service"
            cell.detailTextLabel?.text = LocationManager.shared.authStatusString
        } else if indexPath.section == 1 {
            cell = tableView.dequeueReusableCell(withIdentifier: "StatusTableViewCell", for: indexPath)
            
            cell.textLabel?.text = "Notification Service"
            cell.detailTextLabel?.text = String("Loading...")
            
            DispatchQueue.global().async {
                UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                    DispatchQueue.main.async {
                        if(settings.authorizationStatus == .authorized) {
                            cell.detailTextLabel?.text = String("Authorized")
                        }
                        else {
                            cell.detailTextLabel?.text = String("Not Authorized")
                        }
                    }
                }
            }
        }
        
        return cell
    }
    
}
