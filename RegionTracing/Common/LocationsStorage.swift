//
//  LocationsStorage.swift
//  RegionTracing
//
//  Created by Gautham Velappan on 5/19/20.
//  Copyright © 2020 Gautham Velappan. All rights reserved.
//

import Foundation
import CoreLocation
import UserNotifications

@propertyWrapper
class Persisted<Value: Codable> {
    
    init(userDefaultsKey: String, notificationName: Notification.Name, defaultValue: Value) {
        self.userDefaultsKey = userDefaultsKey
        self.notificationName = notificationName
        if let data = UserDefaults.standard.data(forKey: userDefaultsKey) {
            do {
                wrappedValue = try JSONDecoder().decode(Value.self, from: data)
            } catch {
                wrappedValue = defaultValue
            }
        } else {
            wrappedValue = defaultValue
        }
    }
    
    let userDefaultsKey: String
    let notificationName: Notification.Name
    
    var wrappedValue: Value {
        didSet {
            UserDefaults.standard.set(try! JSONEncoder().encode(wrappedValue), forKey: userDefaultsKey)
            NotificationCenter.default.post(name: notificationName, object: nil)
        }
    }
    
    var projectedValue: Persisted<Value> { self }
    
    func addObserver(using block: @escaping () -> Void) -> NSObjectProtocol {
        return NotificationCenter.default.addObserver(forName: notificationName, object: nil, queue: nil) { _ in
            block()
        }
    }
}

class LocationsStorage {
    static let shared = LocationsStorage()
    
    var sortedLocations: [Location] {
        return locations.sorted { $0.date.compare($1.date) == .orderedDescending }
    }
    
    private(set) var locations: [Location]
    private let fileManager: FileManager
    private let documentsURL: URL
    
    @Persisted(userDefaultsKey: "isOnboarded", notificationName: .init("LocalStoreIsOnboardedDidChange"), defaultValue: false)
    var isOnboarded: Bool

    init() {
        let fileManager = FileManager.default
        documentsURL = try! fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        self.fileManager = fileManager
        
        let jsonDecoder = JSONDecoder()
        
        let locationFilesURLs = try! fileManager.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
        locations = locationFilesURLs.compactMap { url -> Location? in
            guard !url.absoluteString.contains(".DS_Store") else {
                return nil
            }
            guard let data = try? Data(contentsOf: url) else {
                return nil
            }
            return try? jsonDecoder.decode(Location.self, from: data)
        }.sorted(by: { $0.date < $1.date })
    }
    
    func saveLocationOnDisk(_ location: Location) {
        let encoder = JSONEncoder()
        let timestamp = location.date.timeIntervalSince1970
        let fileURL = documentsURL.appendingPathComponent("\(timestamp)")
        
        let data = try! encoder.encode(location)
        try! data.write(to: fileURL)
        
        locations.append(location)
        
        NotificationCenter.default.post(name: .newLocationSaved, object: self, userInfo: ["location": location])
    }
    
    func newVisitReceived(_ visit: CLVisit, description: String, isFake: Bool) {
        let location = Location(visit: visit, descriptionString: description, isFake: isFake)
        LocationsStorage.shared.saveLocationOnDisk(location)
        
        let content = UNMutableNotificationContent()
        content.title = "New Journal entry 📌"
        content.body = location.description
        content.sound = UNNotificationSound.default
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest(identifier: location.dateString, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    func saveCLLocationToDisk(_ clLocation: CLLocation) {
        let currentDate = Date()
        LocationManager.shared.geoCoder.reverseGeocodeLocation(clLocation) { placemarks, _ in
            if let place = placemarks?.first {
                let location = Location(clLocation.coordinate, date: currentDate, descriptionString: "\(place)", isFake: false)
                self.saveLocationOnDisk(location)
            }
        }
    }
}

extension Notification.Name {
    static let newLocationSaved = Notification.Name("newLocationSaved")
}
