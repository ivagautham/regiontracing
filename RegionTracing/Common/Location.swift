//
//  Location.swift
//  RegionTracing
//
//  Created by Gautham Velappan on 5/19/20.
//  Copyright © 2020 Gautham Velappan. All rights reserved.
//

import Foundation
import CoreLocation

class Location: Codable {
    static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .medium
        return formatter
    }()
    
    var coordinates: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    let latitude: Double
    let longitude: Double
    let date: Date
    let dateString: String
    let description: String
    let isFake: Bool
    
    init(_ location: CLLocationCoordinate2D, date: Date, descriptionString: String, isFake: Bool) {
        latitude =  location.latitude
        longitude =  location.longitude
        self.date = date
        dateString = Location.dateFormatter.string(from: date)
        description = descriptionString
        self.isFake = isFake
    }
    
    convenience init(visit: CLVisit, descriptionString: String, isFake: Bool) {
        self.init(visit.coordinate, date: visit.arrivalDate, descriptionString: descriptionString, isFake: isFake)
    }
}
