//
//  LocationManager.swift
//  RegionTracing
//
//  Created by Gautham Velappan on 5/19/20.
//  Copyright © 2020 Gautham Velappan. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject {
    
    static var shared = LocationManager()
    
    var isLocationEnabled: Bool {
        switch authStatus {
        case .notDetermined, .restricted, .denied:
            return false
        case .authorizedAlways, .authorizedWhenInUse:
            return true
        @unknown default:
            return false
        }
    }
    
    var authStatusString: String {
        switch authStatus {
        case .notDetermined: return String("Not Determinded")
        case .restricted: return String("Restricted")
        case .denied: return String("Denied")
        case .authorizedAlways: return String("Always")
        case .authorizedWhenInUse: return String("When In Use")
        @unknown default: return String()
        }
    }
    
    var authStatus: CLAuthorizationStatus {
        return CLLocationManager.authorizationStatus()
    }
    
    let locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        return manager
    }()
    
    let geoCoder = CLGeocoder()
    
    let regionRadius = 1.0
    
    override init() {
        super.init()
        
        locationManager.delegate = self
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        //manager.startMonitoringVisits()
        
        // Uncomment following code to enable fake visits
        locationManager.distanceFilter = 35 // 0
        locationManager.allowsBackgroundLocationUpdates = true // 1
        locationManager.startUpdatingLocation()  // 2
        
    }
    
    func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit) {
        // create CLLocation from the coordinates of CLVisit
        let clLocation = CLLocation(latitude: visit.coordinate.latitude, longitude: visit.coordinate.longitude)
        
        // Get location description
        geoCoder.reverseGeocodeLocation(clLocation) { placemarks, _ in
            if let place = placemarks?.first {
                let description = place.areasOfInterest?.first ?? place.name ?? "\(place)"
                LocationsStorage.shared.newVisitReceived(visit, description: description, isFake: false)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        geoCoder.reverseGeocodeLocation(location) { placemarks, _ in
            if let place = placemarks?.first {
                let description = place.areasOfInterest?.first ?? place.name ?? "\(place)"
                let fakeVisit = FakeVisit(coordinates: location.coordinate, arrivalDate: Date(), departureDate: Date())
                LocationsStorage.shared.newVisitReceived(fakeVisit, description: description, isFake: true)
            }
        }
    }
}

final class FakeVisit: CLVisit {
    private let myCoordinates: CLLocationCoordinate2D
    private let myArrivalDate: Date
    private let myDepartureDate: Date
    
    override var coordinate: CLLocationCoordinate2D {
        return myCoordinates
    }
    
    override var arrivalDate: Date {
        return myArrivalDate
    }
    
    override var departureDate: Date {
        return myDepartureDate
    }
    
    init(coordinates: CLLocationCoordinate2D, arrivalDate: Date, departureDate: Date) {
        myCoordinates = coordinates
        myArrivalDate = arrivalDate
        myDepartureDate = departureDate
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
