//
//  VisitsTableViewController.swift
//  RegionTracing
//
//  Created by Gautham Velappan on 5/21/20.
//  Copyright © 2020 Gautham Velappan. All rights reserved.
//

import UIKit

class VisitsTableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(newLocationAdded(_:)), name: .newLocationSaved, object: nil)
        
        tableView.tableFooterView = UIView()
    }
    
    @objc func newLocationAdded(_ notification: Notification) {
        UIView.performWithoutAnimation {
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return String("Your recent visits")
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let visit = LocationsStorage.shared.sortedLocations
        return visit.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(60.0)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VisitsTableViewCell", for: indexPath)
        
        let visit = LocationsStorage.shared.sortedLocations[indexPath.row]
        cell.textLabel?.text = visit.description
        cell.detailTextLabel?.text = visit.dateString
        
        return cell
    }
    
}
