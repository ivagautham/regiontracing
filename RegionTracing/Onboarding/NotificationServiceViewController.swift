//
//  NotificationServiceViewController.swift
//  RegionTracing
//
//  Created by Gautham Velappan on 5/19/20.
//  Copyright © 2020 Gautham Velappan. All rights reserved.
//

import UIKit
import UserNotifications

class NotificationServiceViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isModalInPresentation = true
    }
    
    @IBAction func onContinue(_ sender: Any) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { granted, error in
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "showConfirmation", sender: nil)
            }
        }
    }
    
    @IBAction func onDontEnable(_ sender: Any) {
        self.performSegue(withIdentifier: "showConfirmation", sender: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
