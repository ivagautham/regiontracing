//
//  ConfirmationViewController.swift
//  RegionTracing
//
//  Created by Gautham Velappan on 5/21/20.
//  Copyright © 2020 Gautham Velappan. All rights reserved.
//

import UIKit

class ConfirmationViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    
        isModalInPresentation = true
    }
    
    @IBAction func onContinue(_ sender: Any) {
        LocationsStorage.shared.isOnboarded = true
        dismiss(animated: true, completion: nil)
    }

}
