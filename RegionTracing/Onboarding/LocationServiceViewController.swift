//
//  LocationServiceViewController.swift
//  RegionTracing
//
//  Created by Gautham Velappan on 5/19/20.
//  Copyright © 2020 Gautham Velappan. All rights reserved.
//

import UIKit
import CoreLocation

class LocationServiceViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        isModalInPresentation = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
         LocationManager.shared.locationManager.delegate = LocationManager.shared
    }

    @IBAction func onContinue(_ sender: Any) {
        LocationManager.shared.locationManager.delegate = self
        LocationManager.shared.locationManager.requestAlwaysAuthorization()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LocationServiceViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status != .notDetermined {
            self.performSegue(withIdentifier: "showNotificationService", sender: nil)
        }
    }
    
}
